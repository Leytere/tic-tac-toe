const soloBtn = document.getElementById('solo-btn');
const multiBtn = document.getElementById('multi-btn');
const infoTxt = document.getElementById('info-text');
const resetBtn = document.getElementById('reset');
const gameContainer = document.getElementById('game-container');
const inputField = document.getElementById('input-field');
let playerTurn = 'no player';
let playMode = 'solo';

let linesNum = 0;
let board = [];



function playTurn(e) {
	let temp;
	let temp2;
	switch (playerTurn) {
		case 'player 1':
		e.target.removeEventListener('click', playTurn);
		e.target.style.backgroundColor = 'blue';
		temp = e.target.id;
		temp2 = temp.slice(7, 8);
		temp = temp.slice(6, 7);
		board[temp][temp2] = 100;
		console.log(board);
		verifyVictory();
		if (playerTurn == 'no player') {
			break;
		}
		if (playMode == 'solo' && playerTurn !== 'no player') {
			infoTxt.innerHTML = "L'Ordinateur calcule son coup...";
			playerTurn = 'computer';
			computerTurn();
		} else if (playerTurn !== 'no player') {
			infoTxt.innerHTML = 'Tour Joueur.se 2';
			playerTurn = 'player 2';
		}
		break;

		case 'player 2':
		e.target.removeEventListener('click', playTurn);
		e.target.style.backgroundColor = 'red';
		temp = e.target.id;
		temp2 = temp.slice(7, 8);
		temp = temp.slice(6, 7);
		board[temp][temp2] = 1000;
		verifyVictory();
		if (playerTurn == 'no player') {
			break;
		}
		infoTxt.innerHTML = 'Tour Joueur.se 1';
		playerTurn = 'player 1';
		break;

		default:
		break;
	}
}

function createBoard(num) {
	let temp = num * num;
	let temp2 = 0;
	for (i = 0; i < num; i++) {
		board.push([]);
		gameContainer.innerHTML += `<div class='row justify-content-center line'></div>`;
	}
	let lines = document.getElementsByClassName('line');
	for (i = 0; i < num; i++) {
		for (j = 0; j < num; j++) {
			board[temp2].push(0);
			lines[j].innerHTML += `<div class='col-2 border square' id='square${j}${temp2}'></div>`;
		}
		temp2++;
	}
	linesNum = num;
	for (i = 0; i < num; i++) {
		for (j = 0; j < num; j++) {
			document.getElementById('square' + i + j).addEventListener('click', playTurn);
		}
	}
	inputField.style.display = 'none';
	soloBtn.style.display = 'block';
	multiBtn.style.display = 'block';
	infoTxt.innerHTML = 'Choisissez votre mode de jeu';
}


function computerTurn() {
	let temp = 0;
	let compChoice = 0;
	let compChoice2 = 0;
	setTimeout(function() {
		for (i = 0; i < linesNum; i++) {
			for(j = 0; j < linesNum; j++) {
				if (board[i - 1] && temp == 0) {
					if (board[i - 2]) {
						if (board[i - 1][j] + board[i - 2][j] == 200 && board[i][j] == 0 || board[i - 1][j] + board[i - 2][j] == 2000 && board[i][j] == 0) {
							board[i][j] = 1000;
							document.getElementById('square' + i + j).style.backgroundColor = 'red';
							document.getElementById('square' + i + j).removeEventListener('click', playTurn);
							temp = 1;
						}
					}
				}
				if (board[i - 1] && temp == 0) {
					if (board[i - 2]) {
						if (board[i - 1][j - 1]) {
							if (board[i - 2][j - 2]) {
								if (board[i - 1][j - 1] + board[i - 2][j - 2] == 200 && board[i][j] == 0 || board[i - 1][j - 1] + board[i - 2][j - 2] == 2000 && board[i][j] == 0) {
									board[i][j] = 1000;
									document.getElementById('square' + i + j).style.backgroundColor = 'red';
									document.getElementById('square' + i + j).removeEventListener('click', playTurn);
									temp = 1;
								}
							}
						}
					}
				}
				if (board[i - 1] && temp == 0) {
					if (board[i - 2]) {
						if (board[i - 1][j + 1]) {
							if (board[i - 2][j + 2]) {
								if (board[i - 1][j + 1] + board[i - 2][j + 2] == 200 && board[i][j] == 0 || board[i - 1][j + 1] + board[i - 2][j + 2] == 2000 && board[i][j] == 0) {
									board[i][j] = 1000;
									document.getElementById('square' + i + j).style.backgroundColor = 'red';
									document.getElementById('square' + i + j).removeEventListener('click', playTurn);
									temp = 1;
								}
							}
						}
					}
				}
				if (board[i][j + 1] && temp == 0) {
					if (board[i][j + 2]) {
						if (board[i][j + 1] + board[i][j + 2] == 200 && board[i][j] == 0 || board[i][j + 1] + board[i][j + 2] == 2000 && board[i][j] == 0) {
							board[i][j] = 1000;
							document.getElementById('square' + i + j).style.backgroundColor = 'red';
							document.getElementById('square' + i + j).removeEventListener('click', playTurn);
							temp = 1;
						} 
					}
				}
				if (board[i][j - 1] && temp == 0) {
					if (board[i][j - 2]) {
						if (board[i][j - 1] + board[i][j - 2] == 200 && board[i][j] == 0 || board[i][j - 1] + board[i][j - 2] == 2000 && board[i][j] == 0) {
							board[i][j] = 1000;
							document.getElementById('square' + i + j).style.backgroundColor = 'red';
							document.getElementById('square' + i + j).removeEventListener('click', playTurn);
							temp = 1;
						}
					}
				}
				if (board[i + 1] && temp == 0) {
					if (board[i + 2]) {
						if (board[i + 1][j] + board[i + 2][j] == 200 && board[i][j] == 0 || board[i + 1][j] + board[i + 2][j] == 2000 && board[i][j] == 0) {
							board[i][j] = 1000;
							document.getElementById('square' + i + j).style.backgroundColor = 'red';
							document.getElementById('square' + i + j).removeEventListener('click', playTurn);
							temp = 1;
						} 
					}
				}
				if (board[i + 1] && temp == 0) {
					if (board[i + 2]) {
						if (board[i + 1][j + 1]) {
							if (board[i + 2][j + 2]) {
								if (board[i + 1][j + 1] + board[i + 2][j + 2] == 200 && board[i][j] == 0 || board[i + 1][j + 1] + board[i + 2][j + 2] == 2000 && board[i][j] == 0) {
									board[i][j] = 1000;
									document.getElementById('square' + i + j).style.backgroundColor = 'red';
									document.getElementById('square' + i + j).removeEventListener('click', playTurn);
									temp = 1;
								}
							}
						}
					}
				}
				if (board[i + 1] && temp == 0) {
					if (board[i + 2]) {
						if (board[i + 1][j - 1]) {
							if (board[i + 2][j - 2]) {
								if (board[i + 1][j - 1] + board[i + 2][j - 2] == 200 && board[i][j] == 0 || board[i + 1][j - 1] + board[i + 2][j - 2] == 2000 && board[i][j] == 0) {
									board[i][j] = 1000;
									document.getElementById('square' + i + j).style.backgroundColor = 'red';
									document.getElementById('square' + i + j).removeEventListener('click', playTurn);
									temp = 1;
								}
							}
						}
					}
				}
				if (board[i + 1] && temp == 0) {
					if (board[i - 1]) {
						if (board[i + 1][j] + board[i - 1][j] == 200 && board[i][j] == 0 || board[i + 1][j] + board[i - 1][j] == 2000 && board[i][j] == 0) {
							board[i][j] = 1000;
							document.getElementById('square' + i + j).style.backgroundColor = 'red';
							document.getElementById('square' + i + j).removeEventListener('click', playTurn);
							temp = 1;
						}
					}
				}
				if (board[i][j - 1] && temp == 0) {
					if (board[i][j + 1]) {
						if (board[i][j - 1] + board[i][j + 1] == 200 && board[i][j] == 0 || board[i][j - 1] + board[i][j + 1] == 2000 && board[i][j] == 0) {
							board[i][j] = 1000;
							document.getElementById('square' + i + j).style.backgroundColor = 'red';
							document.getElementById('square' + i + j).removeEventListener('click', playTurn);
							temp = 1;
						}
					}
				}
				if (board[i + 1] && temp == 0) {
					if (board[i - 1]) {
						if (board[i - 1][j + 1]) {
							if (board[i + 1][j - 1]) {
								if (board[i - 1][j + 1] + board[i + 1][j - 1] == 200 && board[i][j] == 0 || board[i - 1][j + 1] + board[i + 1][j - 1] == 2000 && board[i][j] == 0) {
									board[i][j] = 1000;
									document.getElementById('square' + i + j).style.backgroundColor = 'red';
									document.getElementById('square' + i + j).removeEventListener('click', playTurn);
									temp = 1;
								}
								if (board[i - 1][j - 1] + board[i + 1][j + 1] == 200 && board[i][j] == 0 || board[i - 1][j - 1] + board[i + 1][j + 1] == 2000 && board[i][j] == 0) {
									board[i][j] = 1000;
									document.getElementById('square' + i + j).style.backgroundColor = 'red';
									document.getElementById('square' + i + j).removeEventListener('click', playTurn);
									temp = 1;
								}
							}
						}
					}
				}
			}
		}
		if (temp !== 1) {
			compChoice = Math.floor(Math.random() * linesNum);
			compChoice2 = Math.floor(Math.random() * linesNum);
			for (i = board[compChoice][compChoice2]; i !== 0; i = board[compChoice][compChoice2]) {
				compChoice = Math.floor(Math.random() * linesNum);
				compChoice2 = Math.floor(Math.random() * linesNum);
			}
			board[compChoice][compChoice2] = 1000;
			document.getElementById('square' + compChoice + compChoice2).style.backgroundColor = 'red';
			document.getElementById('square' + compChoice + compChoice2).removeEventListener('click', playTurn);
		}
		infoTxt.innerHTML = 'Tour Joueur.se 1';
		playerTurn = 'player 1';
		verifyVictory();
	}, 800);
}

function verifyVictory() {
	let tie = 0;
	let temp = 0;
	for (i = 0; i < linesNum; i++) {
		for (j = 0; j < linesNum; j++) {
			if (board[i][j + 1] && temp == 0) {
				if (board[i][j + 2]) {
					if (board[i][j] + board[i][j + 1] + board[i][j + 2] == 300) {
						infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					} else if (board[i][j] + board[i][j + 1] + board[i][j + 2] == 3000) {
						if (playMode == 'solo') {
							infoTxt.innerHTML = "L'Ordinateur Gagne !";
						} else {
							infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
						}
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					}
				}
			}
			if (board[i][j - 1] && temp == 0) {
				if (board[i][j - 2]) {
					if (board[i][j] + board[i][j - 1] + board[i][j - 2] == 300) {
						infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					} else if (board[i][j] + board[i][j - 1] + board[i][j - 2] == 3000) {
						if (playMode == 'solo') {
							infoTxt.innerHTML = "L'Ordinateur Gagne !";
						} else {
							infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
						}
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					}
				}
			}
			if (board[i - 1] && temp == 0) {
				if (board[i - 2]) {
					if (board[i][j] + board[i - 1][j] + board[i - 2][j] == 300) {
						infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					} else if (board[i][j] + board[i - 1][j] + board[i - 2][j] == 3000) {
						if (playMode == 'solo') {
							infoTxt.innerHTML = "L'Ordinateur Gagne !";
						} else {
							infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
						}
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					}
				}
			}
			if (board[i + 1] && temp == 0) {
				if (board[i + 2]) {
					if (board[i][j] + board[i + 1][j] + board[i + 2][j] == 300) {
						infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					} else if (board[i][j] + board[i + 1][j] + board[i + 2][j] == 3000) {
						if (playMode == 'solo') {
							infoTxt.innerHTML = "L'Ordinateur Gagne !";
						} else {
							infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
						}
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					}
				}
			}
			if (board[i + 1] && temp == 0) {
				if (board[i + 2]) {
					if (board[i + 1][j - 1]) {
						if (board[i + 2][j - 2]) {
							if (board[i][j] + board[i + 1][j - 1] + board[i + 2][j - 2] == 300) {
								infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							} else if (board[i][j] + board[i + 1][j - 1] + board[i + 2][j - 2] == 3000) {
								if (playMode == 'solo') {
									infoTxt.innerHTML = "L'Ordinateur Gagne !";
								} else {
									infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
								}
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							}
						}
					}
				}
			}
			if (board[i + 1] && temp == 0) {
				if (board[i + 2]) {
					if (board[i + 1][j + 1]) {
						if (board[i + 2][j + 2]) {
							if (board[i][j] + board[i + 1][j + 1] + board[i + 2][j + 2] == 300) {
								infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							} else if (board[i][j] + board[i + 1][j + 1] + board[i + 2][j + 2] == 3000) {
								if (playMode == 'solo') {
									infoTxt.innerHTML = "L'Ordinateur Gagne !";
								} else {
									infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
								}
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							}
						}
					}
				}
			}
			if (board[i - 1] && temp == 0) {
				if (board[i - 2]) {
					if (board[i - 1][j - 1]) {
						if (board[i - 2][j - 2]) {
							if (board[i][j] + board[i - 1][j - 1] + board[i - 2][j - 2] == 300) {
								infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							} else if (board[i][j] + board[i - 1][j - 1] + board[i - 2][j - 2] == 3000) {
								if (playMode == 'solo') {
									infoTxt.innerHTML = "L'Ordinateur Gagne !";
								} else {
									infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
								}
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							}
						}
					}
				}
			}
			if (board[i - 1] && temp == 0) {
				if (board[i - 2]) {
					if (board[i - 1][j + 1]) {
						if (board[i - 2][j + 2]) {
							if (board[i][j] + board[i - 1][j + 1] + board[i - 2][j + 2] == 300) {
								infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							} else if (board[i][j] + board[i - 1][j + 1] + board[i - 2][j + 2] == 3000) {
								if (playMode == 'solo') {
									infoTxt.innerHTML = "L'Ordinateur Gagne !";
								} else {
									infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
								}
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							}
						}
					}
				}
			}
			if (board[i + 1] && temp == 0) {
				if (board[i - 1]) {
					if (board[i][j] + board[i - 1][j] + board[i + 1][j] == 300) {
						infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					} else if (board[i][j] + board[i - 1][j] + board[i + 1][j] == 3000) {
						if (playMode == 'solo') {
							infoTxt.innerHTML = "L'Ordinateur Gagne !";
						} else {
							infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
						}
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					}
				}
			}
			if (board[i][j + 1] && temp == 0) {
				if (board[i][j - 1]) {
					if (board[i][j] + board[i][j + 1] + board[i][j - 1] == 300) {
						infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					} else if (board[i][j] + board[i][j + 1] + board[i][j - 1] == 3000) {
						if (playMode == 'solo') {
							infoTxt.innerHTML = "L'Ordinateur Gagne !";
						} else {
							infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
						}
						playerTurn = 'no player';
						resetBtn.style.display = 'block';
						temp = 1;
						break;
					}
				}
			}
			if (board[i - 1] && temp == 0) {
				if (board[i + 1]) {
					if (board[i - 1][j - 1]) {
						if (board[i + 1][j + 1]) {
							if (board[i][j] + board[i - 1][j - 1] + board[i + 1][j + 1] == 300) {
								infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							} else if (board[i][j] + board[i - 1][j - 1] + board[i + 1][j + 1] == 3000) {
								if (playMode == 'solo') {
									infoTxt.innerHTML = "L'Ordinateur Gagne !";
								} else {
									infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
								}
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							}
							if (board[i][j] + board[i - 1][j + 1] + board[i + 1][j - 1] == 300) {
								infoTxt.innerHTML = 'Joueur.se 1 Gagne !';
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							} else if (board[i][j] + board[i - 1][j + 1] + board[i + 1][j - 1] == 3000) {
								if (playMode == 'solo') {
									infoTxt.innerHTML = "L'Ordinateur Gagne !";
								} else {
									infoTxt.innerHTML = 'Joueur.se 2 Gagne !';
								}
								playerTurn = 'no player';
								resetBtn.style.display = 'block';
								temp = 1;
								break;
							}
						}
					}
				}
			}
		}
	}
	for (i = 0; i < linesNum; i++) {
		for (j = 0; j < linesNum; j++) {
			if (board[i][j] == 100 || board[i][j] == 1000) {
				tie++;
			} 
		}
		if (tie == (linesNum * linesNum)) {
			infoTxt.innerHTML = 'Egalité !';
			playerTurn = 'no player';
			resetBtn.style.display = 'block';
		}
	}
}

function launch() {
	resetBtn.style.display = 'none';
	inputField.style.display = 'block';
	soloBtn.style.display = 'none';
	multiBtn.style.display = 'none';
}

function reset() {
	board = [];
	gameContainer.innerHTML = ``;
	soloBtn.style.display = 'none';
	multiBtn.style.display = 'none';
	infoTxt.innerHTML = 'Choisissez la taille du plateau';
	inputField.style.display = 'block';
	resetBtn.style.display = 'none';
	linesNum = 0;
}

function playModeChoice(choice) {
	soloBtn.style.display = 'none';
	multiBtn.style.display = 'none';
	playerTurn = 'player 1';
	switch (choice) {
		case 'solo':
		infoTxt.innerHTML = `Tour Joueur.se`;
		playMode = 'solo';
		break;

		case 'multi':
		infoTxt.innerHTML = `Tour Joueur.se 1`;
		playMode = 'multi';
		break;

		default:
		break;
	}
}

inputField.addEventListener('change', function(){createBoard(inputField.value)});
resetBtn.addEventListener('click', reset);
window.addEventListener('load', launch);
soloBtn.addEventListener('click', function(){playModeChoice('solo')});
multiBtn.addEventListener('click', function(){playModeChoice('multi')});